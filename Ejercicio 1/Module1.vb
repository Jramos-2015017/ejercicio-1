﻿Module Module1
    WithEvents evaluar As New Evaluator
    Sub Main()

        Do
            Console.WriteLine("Escriba la operacion o 'salir'para terminar")
            Dim operacion = Console.ReadLine
            Try
                Console.WriteLine("Resultado: " & evaluar.Eval(operacion))
                If (operacion.Equals("salir")) Then Exit Do

            Catch ex As Exception
                Console.WriteLine("No es una operacion válida por favor intentelo de nuevo")
            End Try
        Loop
    End Sub

End Module
Public Class Evaluator

    Private mParser As New parser(Me)
    Private mExtraFunctions As Object

    Private Enum eTokenType
        none
        end_of_formula
        operator_plus
        operator_minus
        operator_mul
        operator_div
        open_parenthesis
        close_parenthesis
        value_number
        dot
        comma

    End Enum

    Private Enum ePriority
        none = 0
        [concat] = 1
        [or] = 2
        [and] = 3
        [not] = 4
        equality = 5
        plusminus = 6
        muldiv = 7
        percent = 8
        unaryminus = 9
    End Enum

    Public Class parserexception
        Inherits Exception

        Friend Sub New(ByVal str As String)
            MyBase.New(str)
        End Sub

    End Class

    Private Class tokenizer
        Private mString As String
        Private mLen As Integer
        Private mPos As Integer
        Private mCurChar As Char
        Public startpos As Integer
        Public type As eTokenType
        Public value As New System.Text.StringBuilder
        Private mParser As parser
        Sub New(ByVal Parser As parser, ByVal str As String)
            mString = str
            mLen = str.Length
            mPos = 0
            mParser = Parser
            NextChar()
        End Sub
        Sub NextChar()
            If mPos < mLen Then
                mCurChar = mString.Chars(mPos)
                mPos += 1
            Else
                mCurChar = Nothing
            End If
        End Sub

        Public Function IsOp() As Boolean
            Return mCurChar = "+"c Or mCurChar = "-"c _
               Or mCurChar = "%"c Or mCurChar = "/"c _
               Or mCurChar = "("c Or mCurChar = ")"c _
               Or mCurChar = "."c
        End Function

        Public Sub NextToken()
            startpos = mPos
            value.Length = 0
            type = eTokenType.none
            Do
                Select Case mCurChar
                    Case Nothing
                        type = eTokenType.end_of_formula
                    Case "0"c To "9"c
                        ParseNumber()
                    Case "-"c
                        NextChar()
                        type = eTokenType.operator_minus
                    Case "+"c
                        NextChar()
                        type = eTokenType.operator_plus
                    Case "*"c
                        NextChar()
                        type = eTokenType.operator_mul
                    Case "/"c
                        NextChar()
                        type = eTokenType.operator_div
                    Case "("c
                        NextChar()
                        type = eTokenType.open_parenthesis
                    Case ")"c
                        NextChar()
                        type = eTokenType.close_parenthesis
                    Case ","c
                        NextChar()
                        type = eTokenType.comma
                    Case "."c
                        NextChar()
                        type = eTokenType.dot
                    Case Chr(0) To " "c

                    Case Else

                End Select
                If type <> eTokenType.none Then Exit Do
                NextChar()
            Loop
        End Sub

        Public Sub ParseNumber()
            type = eTokenType.value_number
            While mCurChar >= "0"c And mCurChar <= "9"c
                value.Append(mCurChar)
                NextChar()
            End While
            If mCurChar = "."c Then
                value.Append(mCurChar)
                NextChar()
                While mCurChar >= "0"c And mCurChar <= "9"c
                    value.Append(mCurChar)
                    NextChar()
                End While
            End If
        End Sub

        Public Sub ParseString(ByVal InQuote As Boolean)
            Dim OriginalChar As Char
            If InQuote Then
                OriginalChar = mCurChar
                NextChar()
            End If
            If InQuote Then
            End If
        End Sub

    End Class

    Private Class parser
        Dim tokenizer As tokenizer
        Private mEvaluator As Evaluator
        Sub New(ByVal evaluator As Evaluator)
            mEvaluator = evaluator
        End Sub

        Friend Function ParseExpr(ByVal Acc As Object, ByVal priority As ePriority) As Object
            Dim ValueLeft, valueRight As Object
            Do
                Select Case tokenizer.type
                    Case eTokenType.operator_minus
                        tokenizer.NextToken()
                        ValueLeft = ParseExpr(0, ePriority.unaryminus)
                        If TypeOf ValueLeft Is Double Then
                            ValueLeft = -DirectCast(ValueLeft, Double)
                        Else
                        End If
                        Exit Do
                    Case eTokenType.operator_plus
                        tokenizer.NextToken()

                        Exit Do
                        Exit Do
                    Case eTokenType.value_number
                        ValueLeft = Double.Parse(tokenizer.value.ToString)
                        tokenizer.NextToken()
                        Exit Do
                    Case eTokenType.open_parenthesis
                        tokenizer.NextToken()
                        ValueLeft = ParseExpr(0, ePriority.none)
                        If tokenizer.type = eTokenType.close_parenthesis Then
                            tokenizer.NextToken()
                            Exit Do
                        Else

                        End If
                    Case Else
                        Exit Do
                End Select
            Loop
            Do
                Dim tt As eTokenType
                tt = tokenizer.type
                Select Case tt
                    Case eTokenType.end_of_formula
                        Return ValueLeft
                    Case eTokenType.value_number
                        Exit Function
                    Case eTokenType.operator_plus
                        If priority < ePriority.plusminus Then
                            tokenizer.NextToken()
                            valueRight = ParseExpr(ValueLeft, ePriority.plusminus)
                            If TypeOf ValueLeft Is Double _
                        And TypeOf valueRight Is Double Then
                                ValueLeft = CDbl(ValueLeft) + CDbl(valueRight)
                            ElseIf (TypeOf ValueLeft Is DateTime And TypeOf valueRight Is Double) Then
                                ValueLeft = CDate(ValueLeft).AddDays(CDbl(valueRight))
                            ElseIf (TypeOf ValueLeft Is Double And TypeOf valueRight Is DateTime) Then
                                ValueLeft = CDate(valueRight).AddDays(CDbl(ValueLeft))
                            Else
                                ValueLeft = ValueLeft.ToString & valueRight.ToString
                            End If
                        Else
                            Exit Do
                        End If
                    Case eTokenType.operator_minus
                        If priority < ePriority.plusminus Then
                            tokenizer.NextToken()
                            valueRight = ParseExpr(ValueLeft, ePriority.plusminus)
                            If TypeOf ValueLeft Is Double _
                        And TypeOf valueRight Is Double Then
                                ValueLeft = CDbl(ValueLeft) - CDbl(valueRight)
                            ElseIf (TypeOf ValueLeft Is DateTime And TypeOf valueRight Is Double) Then
                                ValueLeft = CDate(valueRight).AddDays(-CDbl(ValueLeft))
                            ElseIf (TypeOf ValueLeft Is DateTime And TypeOf valueRight Is DateTime) Then
                                ValueLeft = CDate(ValueLeft).Subtract(CDate(valueRight)).TotalDays
                            Else
                            End If
                        Else
                            Exit Do
                        End If
                    Case eTokenType.operator_mul, eTokenType.operator_div
                        If priority < ePriority.muldiv Then
                            tokenizer.NextToken()
                            valueRight = ParseExpr(ValueLeft, ePriority.muldiv)

                            If TypeOf ValueLeft Is Double _
                        And TypeOf valueRight Is Double Then
                                If tt = eTokenType.operator_mul Then
                                    ValueLeft = CDbl(ValueLeft) * CDbl(valueRight)
                                Else
                                    ValueLeft = CDbl(ValueLeft) / CDbl(valueRight)
                                End If
                            Else
                            End If
                        Else
                            Exit Do
                        End If
                    Case Else
                        Exit Do
                End Select
            Loop

            Return ValueLeft
        End Function

        Public Function Eval(ByVal str As String) As Object
            If Len(str) > 0 Then
                tokenizer = New tokenizer(Me, str)
                tokenizer.NextToken()
                Dim res As Object = ParseExpr(Nothing, ePriority.none)

                If tokenizer.type = eTokenType.end_of_formula Then

                    Return res
                Else

                End If
            End If
        End Function

        Public Function EvalString(ByVal str As String) As String
            If Len(str) > 0 Then
                tokenizer = New tokenizer(Me, str)
                tokenizer.ParseString(False)
                Return tokenizer.value.ToString
            Else
                Return String.Empty
            End If
        End Function

    End Class
    Public Function EvalDouble(ByRef formula As String) As Double
        Dim res As Object = Eval(formula)
        If TypeOf res Is Double Then
            Return CDbl(res)
        Else
        End If
    End Function

    Public Function Eval(ByVal str As String) As Object
        str = str.Replace(" ", "")
        str = str.Replace(".", ",")
        Return mParser.Eval(str)
    End Function

    Public Function EvalString(ByVal str As String) As String
        Return mParser.EvalString(str)
    End Function
    Event GetVariable(ByVal name As String, ByRef value As Object)
End Class